﻿using System;
using System.Collections.Generic;

namespace Module3_2
{  
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            result = 1;
            try
            {
                Convert.ToInt32(input);
                if (Convert.ToInt32(input) < 0) throw new ArgumentException("Number not natiral");
                    
            }
            catch
            {
                result = 0;
            }
            return Convert.ToBoolean(result);

        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] mass = new int[n];
            if (n == 2)
            {
                mass[0] = 0;
                mass[1] = 1;
                return mass;
            }
            int p1 = 0, p2 = 1, sum;
            for(int i=2;i<n;i++)
            {
                mass[0] = 0;
                mass[1] = 1;
                sum = p1 + p2;
                mass[i] = sum;
                p1 = p2;
                p2 = sum;
             }
            return mass;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            string numberinstring = Convert.ToString(sourceNumber);
            string reversenumber = "";
            int j = 0;
            int i;
            for (i = numberinstring.Length - 1; i >= 0; i--)
            {
                reversenumber = reversenumber.Insert(j, Convert.ToString(numberinstring[i]));
                j++;
            }
            try
            {
                Convert.ToInt32(reversenumber);
            }
            catch
            {
                reversenumber = reversenumber.Remove(j - 1);
                reversenumber = reversenumber.Insert(0, "-");
                Convert.ToInt32(reversenumber);

            }
            return Convert.ToInt32(reversenumber);
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
         if(size<0)
            { size = 0;
            }
            int[] mass = new int[size];
            Random rand = new Random();
            for (int i = 0; i < mass.Length; i++)
            {
                mass[i] = rand.Next(-10, 10);
            }
            return mass;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            { source[i] *= -1;
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
          if(size<0)
            {
             size = 0;
            }
            int[] mass = new int[size];
            Random rand = new Random();
            for (int i = 0; i < mass.Length; i++)
            {
                mass[i] = rand.Next(-10, 10);
            }
            return mass;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> elements = new List<int> { };
            for (int i = 1; i < source.Length; i++)
            { if (source[i - 1] < source[i])
                {
                    elements.Add(source[i]);
                }
            }
            return elements;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] mass = new int[size, size];
            int i = 1, j, k, t = size / 2;
            for (k = 1; k <= t; k++)
            {
                for (j = k - 1; j < size - k + 1; j++)
                {
                    mass[k - 1, j] = i++;
                }
                for (j = k; j < size - k + 1; j++)
                {
                    mass[j, size - k] = i++;
                }
                for (j = size - k - 1; j >= k - 1; --j)
                {
                    mass[size - k, j] = i++;
                }
                for (j = size - k - 1; j >= k; j--)
                {
                    mass[j, k - 1] = i++;
                }
            }
            return mass;

        }
    }
}
